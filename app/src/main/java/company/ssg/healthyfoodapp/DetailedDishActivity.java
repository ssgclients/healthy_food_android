package company.ssg.healthyfoodapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailedDishActivity extends AppCompatActivity {
    public static final String TAG = "DetailedDishActivity";

    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView imageViewDishImage;
    ImageView imageViewShadow;
    Toolbar toolbar;
    TextView textViewRating;
    TextView textViewCountClient;
    RatingBar ratingBarRating;
    TextView textViewPrice;
    Button buttonShowIngredients;
    //NestedScrollView nestedScrollViewIngredients;

    ArrayAdapter<String> ingredientsAdapter;
    ListView listViewIngredients;
    TextView textViewRestaurantName;
    TextView textViewRestaurantAddress;
    GoogleMap googleMapFragmentMap;
    ImageView imageViewFacebook;
    ImageView imageViewInstagram;
    ImageView imageViewTwitter;

    NestedScrollView mainScrollView;
    ImageView transparentImageView;

    Activity activity;
    //DishWithRestaurant recommendation;
    Recommendation recommendation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_dish);
        Log.i(TAG, "onCreate");

        activity = this;
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            int index = extras.getInt("listIndex", -1);
            if(index != -1 && HF.recommendations != null){
                recommendation = HF.recommendations.get(index);
            }else{
                String id = extras.getString("dishId", "");
                if(!id.isEmpty() && HF.recommendations != null){
                    int length = HF.recommendations.size();
                    for(int j=0; j<length; j++){
                        if(HF.recommendations.get(j).plate._id.equals(id)){
                            recommendation = HF.recommendations.get(j);
                            break;
                        }
                    }
                }
            }
        }

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.DetailedDishActivityCollapsingToolbarLayoutToolbar);
        imageViewDishImage = (ImageView)findViewById(R.id.DetailedDishActivityImageViewDishImage);
        imageViewShadow = (ImageView)findViewById(R.id.DetailedDishActivityImageViewShadow);
        toolbar = (Toolbar) findViewById(R.id.DetailedDishActivityToolbar);
        setSupportActionBar(toolbar);

        mainScrollView = (NestedScrollView) findViewById(R.id.DetailedDishActivityNestedScrollViewMainScrollView);
        textViewRating = (TextView)findViewById(R.id.DetailedDishActivityTextViewRating);
        textViewCountClient = (TextView)findViewById(R.id.DetailedDishActivityTextViewCountClients);
        ratingBarRating = (RatingBar)findViewById(R.id.DetailedDishActivityRatingBarRating);
        ratingBarRating.setOnRatingBarChangeListener(onRatingBarChangeListener);

        textViewPrice = (TextView)findViewById(R.id.DetailedDishActivityTextViewPrice);

        buttonShowIngredients = (Button)findViewById(R.id.DetailedDishActivityButtonShowIngredients);
        buttonShowIngredients.setOnClickListener(onClickButtonShowIngredients);
        listViewIngredients = (ListView)findViewById(R.id.DetailedDishActivityListViewIngredients);
        listViewIngredients.setOnTouchListener(onTouchListViewIngredients);
        textViewRestaurantName = (TextView)findViewById(R.id.DetailedDishActivityTextViewRestaurantName);
        textViewRestaurantAddress = (TextView)findViewById(R.id.DetailedDishActivityTextViewRestaurantAddress);

        ((MapFragment) getFragmentManager().findFragmentById(R.id.DetailedDishActivityFragmentMap)).getMapAsync(onMapReady);
        transparentImageView = (ImageView) findViewById(R.id.DetailedDishActivityTransparentImage);
        transparentImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mainScrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        imageViewFacebook = (ImageView)findViewById(R.id.DetailedDishActivityImageViewFacebook);
        imageViewFacebook.setOnClickListener(onClickImageViewFacebook);

        imageViewInstagram = (ImageView)findViewById(R.id.DetailedDishActivityImageViewInstagram);
        imageViewInstagram.setOnClickListener(onClickImageViewInstagram);

        imageViewTwitter= (ImageView)findViewById(R.id.DetailedDishActivityImageViewTwitter);
        imageViewTwitter.setOnClickListener(onClickImageViewTwitter);

        setDishData();
    }

    private RatingBar.OnRatingBarChangeListener onRatingBarChangeListener = new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, final float rating, boolean fromUser) {
            if (fromUser){
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailedDishActivity.this);
                builder.setMessage(getString(R.string.phrase_ratingDishConfirmation)+" "+String.valueOf(rating)+"?");
                builder.setTitle(getString(R.string.word_confirmation));
                builder.setPositiveButton(getString(R.string.word_yes), new DialogInterface.OnClickListener()  {
                    public void onClick(DialogInterface dialog, int id) {
                        if(recommendation != null){
                            Log.i(TAG, "Rating recommendation with id: " + String.valueOf(recommendation.plate._id));
                            AttemptUpdateDishRating task = new AttemptUpdateDishRating(DetailedDishActivity.this);
                            task.token = HF.UserAccessToken;
                            task.dishId = recommendation.plate._id;
                            task.dishRating = String.valueOf(rating);
                            task.execute();
                        }
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create();
                builder.show();
            }
        }
    };



    private OnMapReadyCallback onMapReady = new  OnMapReadyCallback() {
        @Override
        public void onMapReady (GoogleMap googleMap){
            Log.i(TAG, "onMapReady");
            googleMapFragmentMap = googleMap;
            if(recommendation != null){
                LatLng position = new LatLng(recommendation.plate.restaurant.localization.coordinates.get(0), recommendation.plate.restaurant.localization.coordinates.get(1));
                googleMapFragmentMap.addMarker(new MarkerOptions().position(position).title("Ubicacion del restaurante"));
                googleMapFragmentMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16.0f));
            }
        }
    };

    private View.OnClickListener onClickButtonShowIngredients = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(listViewIngredients.getVisibility() == View.GONE) {
                listViewIngredients.setVisibility(View.VISIBLE);
                buttonShowIngredients.setText(getString(R.string.phrase_hideIngredients));
            }else if(listViewIngredients.getVisibility() == View.VISIBLE) {
                listViewIngredients.setVisibility(View.GONE);
                buttonShowIngredients.setText(getString(R.string.phrase_showIngredients));
            }
        }
    };

    private View.OnTouchListener onTouchListViewIngredients = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    public void setRatingDishData(){
        if(recommendation != null){
            float rating = 0;
            if(recommendation.plate.rating.countClients != 0){
                rating = recommendation.plate.rating.sum / recommendation.plate.rating.countClients;
            }
            textViewRating.setText(" "+String.valueOf(rating));
            textViewCountClient.setText(" "+String.valueOf(recommendation.plate.rating.countClients));
            AttemptGetDishRating task = new AttemptGetDishRating(this);
            task.token = HF.UserAccessToken;
            task.userId = HF.UserId;
            task.dishId = recommendation.plate._id;
            task.execute();
        }
    }

    public void setDishData(){
        if(recommendation != null){
            collapsingToolbarLayout.setTitle(recommendation.plate.name);
            //collapsingToolbarLayout.setSubtitle(recommendation.restaurant.name);
            Log.i(TAG, recommendation.plate.image.url);
            if(recommendation.plate.image != null) {
                Glide.with(DetailedDishActivity.this)
                        .load(recommendation.plate.image.url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                Log.i(TAG, "onResourceReady");
                                imageViewDishImage.setImageBitmap(bitmap);
                                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                                    @Override
                                    public void onGenerated(Palette palette) {
                                        int vibrantColor = palette.getVibrantColor(getResources().getColor(R.color.primaryDark));
                                        collapsingToolbarLayout.setContentScrimColor(vibrantColor);
                                        imageViewShadow.setBackgroundColor(vibrantColor);
                                        imageViewShadow.getBackground().setAlpha(80);
                                    }
                                });
                            }
                        });
            }
            setRatingDishData();
            textViewPrice.setText("$ "+String.valueOf(recommendation.plate.price));
            ingredientsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, recommendation.plate.ingredients);
            listViewIngredients.setAdapter(ingredientsAdapter);

            textViewRestaurantName.setText(recommendation.plate.restaurant.name);
            textViewRestaurantAddress.setText(recommendation.plate.restaurant.address);
        }
    }

    private View.OnClickListener onClickImageViewFacebook = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(recommendation != null){
                if(recommendation.plate.restaurant != null && recommendation.plate.restaurant.social != null && !recommendation.plate.restaurant.social.facebook.isEmpty()){
                    Log.i(TAG, recommendation.plate.restaurant.social.facebook);
                    try {
                        String uri = recommendation.plate.restaurant.social.facebook;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(DetailedDishActivity.this, "Este restaurante no tiene cuenta de facebook asociada", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(DetailedDishActivity.this, "Este restaurante no tiene cuenta de facebook asociada", Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    private View.OnClickListener onClickImageViewTwitter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "onClickImageViewTwitter");
            if(recommendation != null){
                if(recommendation.plate.restaurant != null && recommendation.plate.restaurant.social != null && !recommendation.plate.restaurant.social.twitter.isEmpty()){
                    String uri = "https://twitter.com/#!/"+ recommendation.plate.restaurant.social.twitter;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(new Intent(intent));
                }else{
                    Toast.makeText(DetailedDishActivity.this, "Este restaurante no tiene cuenta de twitter asociada", Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    private View.OnClickListener onClickImageViewInstagram = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(recommendation != null){
                if(recommendation.plate.restaurant != null && recommendation.plate.restaurant.social != null && !recommendation.plate.restaurant.social.instagram.isEmpty()){
                    Uri uri = Uri.parse("http://instagram.com/_u/"+ recommendation.plate.restaurant.social.instagram);
                    Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                    likeIng.setPackage("com.instagram.android");
                    try {
                        startActivity(likeIng);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram" + ".com/"+ recommendation.plate.restaurant.social.instagram)));
                    }
                }else{
                    Toast.makeText(DetailedDishActivity.this, "Este restaurante no tiene cuenta de instagram asociada", Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    public class AttemptUpdateDishRating extends UpdateDishRatingTask {
        ProgressDialog progressDialog;

        public AttemptUpdateDishRating(Context ctx){
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DetailedDishActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_attemptRating));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, HttpResponse<Dish>> result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                if(result.data != null){
                    switch (result.data.code) {
                        case 200 :
                            if(result.data.data != null){
                                recommendation.plate.rating = result.data.data.rating;
                                setRatingDishData();
                                Toast.makeText(getApplicationContext(), getString(R.string.phrase_ratingUpdates), Toast.LENGTH_SHORT).show();
                            }
                            break;
                        default :
                            Toast.makeText(getApplicationContext(), String.valueOf(result.data.code), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }
    }

    public class AttemptGetDishRating extends GetDishRatingTask {

        public AttemptGetDishRating(Context ctx){
            super(ctx);
        }

        @Override
        protected void onPostExecute(Res<Error, HttpResponse<Float>> result) {
            super.onPostExecute(result);
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                if (result.data != null){
                    switch (result.data.code) {
                        case 200 :
                            if(result.data.data != null){
                                ratingBarRating.setRating(result.data.data);
                            }
                            break;
                        default:
                            Toast.makeText(getApplicationContext(), String.valueOf(result.data.code), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }
    }
}
