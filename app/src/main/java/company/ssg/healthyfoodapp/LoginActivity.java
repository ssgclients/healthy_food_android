package company.ssg.healthyfoodapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = "LoginActivity";

    SharedPreferences sharedPreferences;

    EditText editTextEmail;
    EditText editTextPassword;
    Button buttonLogin;

    TextView linkSingup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        linkSingup = (TextView)findViewById(R.id.LoginActivityTextViewSignUpLink);
        linkSingup.setOnClickListener(onClickSignUpLink);

        editTextEmail = (EditText)findViewById(R.id.LoginActivityEditTextEmail);
        editTextPassword = (EditText)findViewById(R.id.LoginActivityEditTextPassword);

        buttonLogin = (Button)findViewById(R.id.LoginActivityButtonLogin);
        buttonLogin.setOnClickListener(onClickButtonLogin);

        sharedPreferences = getSharedPreferences(HF.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        /*HF.DeviceId = sharedPreferences.getString("deviceId", "");
        Log.i(TAG, "Device id: " + HF.DeviceId);
        if(HF.DeviceId.isEmpty()){
            Log.i(TAG, "Getting deviceId from settings");
            String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            HF.DeviceId = deviceId;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("deviceId", deviceId);
            editor.apply();
        }*/

        HF.UserAccessToken = this.sharedPreferences.getString("userAccessToken", "");
        HF.UserEmail = this.sharedPreferences.getString("userEmail", "");

        Log.i(TAG, "Access token: " + HF.UserAccessToken);
        if(HF.UserAccessToken != ""){
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private View.OnClickListener onClickButtonLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String email = editTextEmail.getText().toString();
            String password = editTextPassword.getText().toString();
            if(!email.isEmpty() && !password.isEmpty()){
                AttemptLogin task =  new AttemptLogin(LoginActivity.this);
                task.email = email;
                task.password = password;
                task.execute();
            }else{
                Toast.makeText(LoginActivity.this, R.string.phrase_login_emptyCredentials, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener onClickSignUpLink = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, " Link clicked");
            Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
            startActivity(i);
            finish();
        }
    };

    public class AttemptLogin extends LoginTask {
        ProgressDialog progressDialog;

        public AttemptLogin(Context ctx){
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_login_authenticating));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, Integer> result) {
            super.onPostExecute(result);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                switch (result.data) {
                    case 200 :
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case 401 :
                        Toast.makeText(context, getString(R.string.phrase_login_wrongCredentials), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(context, String.valueOf(result.data), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }
}
