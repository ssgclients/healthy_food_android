package company.ssg.healthyfoodapp;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

class Helpers{
    public static final String TAG = "Helpers";
    public static <T> Res<Error, T> doTry(Callable<Res<Error, T>> func) {
        try {
            return func.call();
        } catch (ConnectException ce) {
            Log.e(TAG, ce.getMessage());
            return new Res<>(new Error("ConnectException", ce.getMessage()), null);
        } catch (UnknownHostException uhe) {
            Log.e(TAG, uhe.getMessage());
            return new Res<>(new Error("UnknownHostException", uhe.getMessage()), null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return new Res<>(new Error("e", e.getMessage()), null);
        }
    }

    public static void responseToError(Context ctx, Error error){
        switch (error.code) {
            case "ConnectException" :
                Toast.makeText(ctx, "Connection exception", Toast.LENGTH_SHORT).show();
                break;
            case "UnknownHostException" :
                Toast.makeText(ctx, "Error tratando de conectarser con el servidor", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(ctx, "Algo salio mal: " + error.message, Toast.LENGTH_SHORT).show();
                break;
        }
        return;
    }
}

public class HF {
    public static final String API_SERVER_HOST = "http://healthyfood.ssg.company";
    //public static final String API_SERVER_HOST = "http://192.168.1.5:4000";
    public static final String SHARED_PREFERENCES = "company.ssg.healthyfoodapp.sharedPreferences";
    //public static final String TAG = "Healthy Food";

    public static String UserEmail = "";
    public static String UserId = "";
    public static String UserAccessToken = "";

    public static String DeviceId = "";

    public static ArrayList<Restaurant> restaurants = null;
    public static ArrayList<Ingredient> ingredients = null;
    public static ArrayList<Disease> diseases = null;
    //public static ArrayList<DishWithRestaurant> dishes = null;
    public static ArrayList<Recommendation> recommendations = null;
    public static Profile<Disease> userProfile = null;

    public static Location MyCurretLocation = null;

    public static Dish dish = null;
}

class Res<E,D> {
    public E error = null;
    public D data = null;

    public Res(E e, D d){
        error = e;
        data = d;
    }
}

class Error {
    public String code = "";
    public String message = "";
    public Error(String  c, String m){
        code = c;
        message = m;
    }
}

class HttpResponse<D> {
    int code = 0;
    D data = null;

    public HttpResponse(int c, D d){
        code = c;
        data = d;
    }
}

class Restaurant {
    public String _id;
    public String name;
    public String phone;
    public String address;
    public String website;
    public Localization localization;
    public Social social;

    class Localization {
        ArrayList<Float> coordinates;
    }

    class Social {
        String instagram;
        String facebook;
        String twitter;
    }
}

class Ingredient {
    public String _id;
    public String name;
}

class Profile<DiseaseType> {
    public String _id;
    public String email = "";
    public Name name = new Name();
    public String phone = "";
    public String address = "";
    public ArrayList<DiseaseType> diseases = new ArrayList<>();

    class Name {
        public String first = "";
        public String last = "";
    }
}

class Disease {
    public String _id;
    public String name;
    public ArrayList<String> noRecommended = new ArrayList<>();
    public ArrayList<String> recommended = new ArrayList<>();
}

class DishWithRestaurant {
    public String _id = "";
    public String name = "";
    public int price = 0;
    public ArrayList<String> ingredients;
    public Image image;
    public Rating rating;
    public Restaurant restaurant;
}

class Dish {
    public String _id = "";
    public String name = "";
    public int price = 0;
    public ArrayList<String> ingredients;
    public Image image;
    public Rating rating;
}

class Recommendation {
    public DishWithRestaurant plate;
    public Distance distance;

    class Distance {
        public String text = "";
        public int value = 0;
    }
}

class Rating {
    public float sum = 0;
    public int countClients = 0;
}

class Image {
    public String pubId;
    public String url;
}

class IngredientsAdapter extends ArrayAdapter<Ingredient> implements Filterable {

    private LayoutInflater mInflater;

    public IngredientsAdapter(final Context context) {
        super(context, -1);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final TextView tv;
        if (convertView != null) {
            tv = (TextView) convertView;
        } else {
            tv = (TextView) mInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        tv.setText(getItem(position).name);
        return tv;
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                ArrayList<Ingredient> ingredientsFilteredList = null;
                ingredientsFilteredList = new ArrayList<>();
                if (constraint != null) {
                    if(HF.ingredients != null){
                        for (Ingredient res : HF.ingredients){
                            if(res.name.toLowerCase().contains(constraint.toString().toLowerCase())){
                                ingredientsFilteredList.add(res);                                    }
                        }
                    }
                }

                final FilterResults filterResults = new FilterResults();
                filterResults.values = ingredientsFilteredList;
                filterResults.count = ingredientsFilteredList.size();
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(final CharSequence contraint, final FilterResults results) {
                clear();
                for (Ingredient res : (ArrayList<Ingredient>) results.values) {
                    add(res);
                }
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(final Object resultValue) {
                return resultValue == null ? "" : ((Ingredient) resultValue).name;
            }
        };
        return myFilter;
    }
}

class RestaurantsAdapter extends ArrayAdapter<Restaurant> implements Filterable {

    private LayoutInflater mInflater;

    public RestaurantsAdapter(final Context context) {
        super(context, -1);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final TextView tv;
        if (convertView != null) {
            tv = (TextView) convertView;
        } else {
            tv = (TextView) mInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }
        Restaurant r = getItem(position);
        tv.setText(r.name);
        return tv;
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                ArrayList<Restaurant> restaurantFilteredList = null;
                restaurantFilteredList = new ArrayList<>();
                if (constraint != null) {
                    if(HF.restaurants != null){
                        for (Restaurant res : HF.restaurants){
                            if(res.name.toLowerCase().contains(constraint.toString().toLowerCase())){
                                restaurantFilteredList.add(res);                                    }
                        }
                    }
                }

                final FilterResults filterResults = new FilterResults();
                filterResults.values = restaurantFilteredList;
                filterResults.count = restaurantFilteredList.size();
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(final CharSequence contraint, final FilterResults results) {
                clear();
                for (Restaurant res : (ArrayList<Restaurant>) results.values) {
                    add(res);
                }
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(final Object resultValue) {
                return resultValue == null ? "" : ((Restaurant) resultValue).name;
            }
        };
        return myFilter;
    }
}
