package company.ssg.healthyfoodapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {
    public static final String TAG = "SignUpActivity";

    EditText editTextEmail;
    EditText editTextPassword;
    EditText editTextConfirmPassword;
    Button buttonSignup;
    TextView linkToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        editTextEmail = (EditText)findViewById(R.id.SignupEditTextEmail);
        editTextPassword = (EditText)findViewById(R.id.SignupEditTextPassword);
        editTextConfirmPassword = (EditText)findViewById(R.id.SignupEditTextConfirmPassword);

        buttonSignup = (Button)findViewById(R.id.SignupButtonSignup);
        buttonSignup.setOnClickListener(onClickButtonSignup);

        linkToLogin = (TextView)findViewById(R.id.SignupTextViewLoginLink);
        linkToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "AttemptLogin view Link clicked");
                Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private View.OnClickListener onClickButtonSignup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String email = editTextEmail.getText().toString();
            String password = editTextPassword.getText().toString();
            String password2 = editTextConfirmPassword.getText().toString();
            if(!email.isEmpty() && !password.isEmpty() && !password2.isEmpty()){
                if(password.equals(password2)){
                    AttemptSignUp task = new AttemptSignUp(SignUpActivity.this);
                    task.email = email;
                    task.password = password;
                    task.execute();
                }else{
                    Toast.makeText(SignUpActivity.this, R.string.phrase_signup_passwordsNotMach, Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(SignUpActivity.this, R.string.phrase_signup_emptyCredentials, Toast.LENGTH_SHORT).show();
            }
        }
    };

    public class AttemptSignUp extends SignUpTask {
        ProgressDialog progressDialog;

        public AttemptSignUp(Context ctx) {
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SignUpActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_SignUp_registering));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, Integer> result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                switch (result.data) {
                    case 201 :
                        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case 409:
                        Toast.makeText(context, getString(R.string.phrase_emailAlreadyRegistered), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(context, String.valueOf(result.data), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }
}
