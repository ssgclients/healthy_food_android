package company.ssg.healthyfoodapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

public class DishesAdapter extends RecyclerView.Adapter<DishesAdapter.ViewHolder> {
    public static final String TAG = "DishesAdapter";
    private ArrayList<Recommendation> recommendations;
    private int idLayout;
    public Context context;
    public Activity activity;

    public DishesAdapter(int rowLayout, Context context, Activity activity) {
        Log.i(TAG, "DishesAdapter");
        this.recommendations = HF.recommendations;
        this.idLayout = rowLayout;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Log.i(TAG, "onCreateViewHolder");
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(idLayout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        Log.i(TAG, "onBindViewHolder");
        Recommendation recommendation = recommendations.get(i);
        viewHolder.setData(recommendation, context);
    }

    @Override
    public int getItemCount() {
        return recommendations == null ? 0 : recommendations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public static final String TAG = "DA.ViewHolder";
        public Context context;
        public Recommendation recommendation;
        public TextView textViewDishName;
        public TextView textViewDishRating;
        public TextView textViewDishCountClients;
        public RatingBar ratingBarDishRating;
        public TextView textViewDishPrice;
        public TextView textViewRestaurantName;
        public TextView textViewRestaurantDistance;
        public ImageView imageViewDishImage;
        public LinearLayout linearLayoutHeaderCard;
        public LinearLayout linearLayoutFooterCard;

        public ViewHolder(View itemView) {
            super(itemView);
            Log.i(TAG, "ViewHolder");
            textViewDishName = (TextView) itemView.findViewById(R.id.DishListItemTextViewDishName);
            textViewDishRating = (TextView) itemView.findViewById(R.id.DishListItemTextViewRating);
            textViewDishCountClients = (TextView) itemView.findViewById(R.id.DishListItemTextViewCountClients);
            ratingBarDishRating = (RatingBar) itemView.findViewById(R.id.DishListItemRatingBarDishRating);
            textViewDishPrice = (TextView) itemView.findViewById(R.id.DishListItemTextViewPrice);
            textViewRestaurantName = (TextView) itemView.findViewById(R.id.DishListItemTextViewRestaurantName);
            textViewRestaurantDistance = (TextView) itemView.findViewById(R.id.DishListItemTextViewRestaurantDistance);
            imageViewDishImage = (ImageView) itemView.findViewById(R.id.DishListItemImageViewDishImage);
            linearLayoutHeaderCard = (LinearLayout) itemView.findViewById(R.id.DishListItemLinearLayoutHeaderCard);
            linearLayoutFooterCard = (LinearLayout) itemView.findViewById(R.id.DishListItemLinearLayoutFooterCard);
            itemView.setOnClickListener(this);
        }

        public void setData(Recommendation r, Context ctx){
            context = ctx;
            recommendation = r;
            textViewDishName.setText(recommendation.plate.name);
            //ratingBarDishRating.setRating(recommendation.rating.sum / recommendation.rating.countClients);
            textViewDishPrice.setText("$" + String.valueOf(recommendation.plate.price));
            textViewRestaurantName.setText(recommendation.plate.restaurant.name);
            if(recommendation.distance != null){
                textViewRestaurantDistance.setText(recommendation.distance.text);
            }
            if(recommendation.plate.image != null){
                Glide.with(context)
                        .load(recommendation.plate.image.url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                imageViewDishImage.setImageBitmap(bitmap);
                                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                                    @SuppressWarnings("ResourceType")
                                    @Override
                                    public void onGenerated(Palette palette) {
                                        int vibrantColor = palette.getVibrantColor(R.color.transparentPrimaryDark);
                                        //int vibrantDarkColor = palette.getDarkVibrantColor(R.color.transparentPrimaryDark);
                                        linearLayoutHeaderCard.setBackgroundColor(vibrantColor);
                                        //linearLayoutHeaderCard.getBackground().setAlpha(160);
                                        linearLayoutFooterCard.setBackgroundColor(vibrantColor);
                                        //linearLayoutFooterCard.getBackground().setAlpha(160);
                                    }
                                });
                            }
                        });
            }
            float rating = 0;
            if(recommendation.plate.rating.countClients != 0){
                rating = recommendation.plate.rating.sum / recommendation.plate.rating.countClients;
            }
            textViewDishRating.setText(" "+String.valueOf(rating));
            textViewDishCountClients.setText(" "+String.valueOf(recommendation.plate.rating.countClients));

            AttemptGetDishRating task = new AttemptGetDishRating(context);
            task.token = HF.UserAccessToken;
            task.userId = HF.UserId;
            task.dishId = recommendation.plate._id;
            task.execute();
        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "onClick");
            int position = getAdapterPosition();
            Intent intent = new Intent(context, DetailedDishActivity.class);
            intent.putExtra("listIndex", position);
            activity.startActivity(intent);
        }

        public class AttemptGetDishRating extends GetDishRatingTask {
            public AttemptGetDishRating(Context ctx){
                super(ctx);
            }

            @Override
            protected void onPostExecute(Res<Error, HttpResponse<Float>> result) {
                super.onPostExecute(result);
                if(result.error != null){
                    Helpers.responseToError(context, result.error);
                }else{
                    if (result.data != null){
                        switch (result.data.code) {
                            case 200 :
                                if(result.data.data != null){
                                    ratingBarDishRating.setRating(result.data.data);
                                }
                                break;
                            default:
                                Toast.makeText(context, String.valueOf(result.data.code), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
            }
        }

    }
}
