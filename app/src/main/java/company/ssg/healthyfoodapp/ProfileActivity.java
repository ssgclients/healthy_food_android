package company.ssg.healthyfoodapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {
    public static final String TAG = "ProfileActivity";
    SharedPreferences sharedPreferences;
    int LIST_VIEW_HEIGHT;

    EditText editTextEmail;
    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextPhone;
    EditText editTextAddress;
    AutoCompleteTextView textViewDisease;
    ImageButton buttonAddDisease;
    ListView listViewDiseases;
    DiseasesAdapter diseasesListViewAdapter;

    Button buttonUpdateProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        LIST_VIEW_HEIGHT =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());

        Toolbar toolbar = (Toolbar) findViewById(R.id.ProfileActivityToolbar);
        setSupportActionBar(toolbar);

        editTextEmail = (EditText)findViewById(R.id.ProfileEditTextEmail);
        editTextFirstName = (EditText)findViewById(R.id.ProfileEditTextFirstName);
        editTextLastName = (EditText)findViewById(R.id.ProfileEditTextLastName);
        editTextPhone = (EditText)findViewById(R.id.ProfileEditTextPhone);
        editTextAddress = (EditText)findViewById(R.id.ProfileEditTextAddress);

        textViewDisease = (AutoCompleteTextView)findViewById(R.id.ProfileTextViewDiseases);
        textViewDisease.setAdapter(new DiseasesAdapter(this));
        buttonAddDisease = (ImageButton)findViewById(R.id.ProfileButtonAddDisease);
        buttonAddDisease.setOnClickListener(onClickButtonAddDisease);
        diseasesListViewAdapter = new DiseasesAdapter(this);
        listViewDiseases = (ListView)findViewById(R.id.ProfileListViewDiseases);
        listViewDiseases.setAdapter(diseasesListViewAdapter);
        listViewDiseases.setOnTouchListener(onTouchListViewDiseases);
        listViewDiseases.setOnItemLongClickListener(onItemDiseaseLongClickListener);

        buttonUpdateProfile = (Button)findViewById(R.id.ProfileButtonUpdateProfile);
        buttonUpdateProfile.setOnClickListener(onClickButtonUpdateProfile);

        if(HF.userProfile != null){
            setProfileData();
        }else{
            GetUserProfileData task = new GetUserProfileData(this);
            task.userId = HF.UserId;
            task.token = HF.UserAccessToken;
            task.execute();
        }
    }

    private void setProfileData(){
        if(HF.userProfile != null){
            editTextEmail.setText(HF.userProfile.email);
            editTextFirstName.setText(HF.userProfile.name.first);
            editTextLastName.setText(HF.userProfile.name.last);
            editTextPhone.setText(HF.userProfile.phone);
            editTextAddress.setText(HF.userProfile.address);
            if(HF.userProfile != null && HF.userProfile.diseases != null){
                int length = HF.userProfile.diseases.size();
                Log.i(TAG, String.valueOf(length));
                if(diseasesListViewAdapter == null){
                    Log.i(TAG, "Initializing diseases adapter");
                    diseasesListViewAdapter = new DiseasesAdapter(this);
                }
                diseasesListViewAdapter.clear();
                diseasesListViewAdapter.addAll(HF.userProfile.diseases);
                if(!diseasesListViewAdapter.isEmpty()){
                    listViewDiseases.setVisibility(View.VISIBLE);
                }
                if(diseasesListViewAdapter.getCount() > 1){
                    listViewDiseases.getLayoutParams().height = LIST_VIEW_HEIGHT;
                }
            }
        }
    }

    private View.OnClickListener onClickButtonAddDisease = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = textViewDisease.getText().toString();
            if(name.isEmpty()){
                Toast.makeText(ProfileActivity.this,
                        getString(R.string.phrase_diseaseNameRequired),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            Disease selectedDisease = null;
            if(HF.diseases != null){
                int length = HF.diseases.size();
                for (int i = 0; i< length; i++){
                    if(HF.diseases.get(i).name.equals(name)){
                        selectedDisease = HF.diseases.get(i);
                    }
                }
            }
            if(selectedDisease == null){
                Toast.makeText(ProfileActivity.this,
                        getString(R.string.phrase_diseaseNameNotFound),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            int length = diseasesListViewAdapter.getCount();
            for (int i = 0; i< length; i++){
                if(diseasesListViewAdapter.getItem(i).name.equals(name)){
                    Toast.makeText(ProfileActivity.this,
                            getString(R.string.phrase_diseaseAlreadyAdded),
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            diseasesListViewAdapter.add(selectedDisease);
            if(!diseasesListViewAdapter.isEmpty()){
                listViewDiseases.setVisibility(View.VISIBLE);
            }
            if(diseasesListViewAdapter.getCount() > 1){
                listViewDiseases.getLayoutParams().height = LIST_VIEW_HEIGHT;
            }
            textViewDisease.setText("");
        }
    };

    private View.OnTouchListener onTouchListViewDiseases = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.i(TAG, "Click");
        }
    };

    private AdapterView.OnItemLongClickListener onItemDiseaseLongClickListener = new AdapterView.OnItemLongClickListener() {
        int  pos = -1;
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            pos = position;
            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
            builder.setMessage(getString(R.string.phrase_deleteDiseaseConfirmation));
            builder.setTitle(getString(R.string.word_confirmation));
            builder.setPositiveButton("Delete", new DialogInterface.OnClickListener()  {
                public void onClick(DialogInterface dialog, int id) {
                    diseasesListViewAdapter.remove(diseasesListViewAdapter.getItem(pos));
                    if(diseasesListViewAdapter.getCount() == 1) {
                        listViewDiseases.getLayoutParams().height = ListView.LayoutParams.WRAP_CONTENT;
                    }
                    if(diseasesListViewAdapter.isEmpty()){
                        listViewDiseases.setVisibility(View.GONE);
                    }
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create();
            builder.show();
            return true;
        }
    };

    private class DiseasesAdapter extends ArrayAdapter<Disease> implements Filterable {

        private LayoutInflater mInflater;

        public DiseasesAdapter(final Context context) {
            super(context, -1);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            final TextView tv;
            if (convertView != null) {
                tv = (TextView) convertView;
            } else {
                tv = (TextView) mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            }
            tv.setText(getItem(position).name);
            return tv;
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(final CharSequence constraint) {
                    ArrayList<Disease> diseasesFilteredList = null;
                    diseasesFilteredList = new ArrayList<>();
                    if (constraint != null) {
                        if(HF.diseases != null){
                            for (Disease disease : HF.diseases){
                                if(disease.name.toLowerCase().contains(constraint.toString().toLowerCase())){
                                    diseasesFilteredList.add(disease);                                    }
                            }
                        }
                    }
                    final FilterResults filterResults = new FilterResults();
                    filterResults.values = diseasesFilteredList;
                    filterResults.count = diseasesFilteredList.size();
                    return filterResults;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(final CharSequence contraint, final FilterResults results) {
                    clear();
                    for (Disease disease : (ArrayList<Disease>) results.values) {
                        add(disease);
                    }
                    if (results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }

                @Override
                public CharSequence convertResultToString(final Object resultValue) {
                    return resultValue == null ? "" : ((Disease) resultValue).name;
                }
            };
            return myFilter;
        }
    }

    private View.OnClickListener onClickButtonUpdateProfile = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
            builder.setMessage(getString(R.string.phrase_profile_confirmation));
            builder.setTitle(getString(R.string.word_confirmation));
            builder.setPositiveButton(getString(R.string.word_update), new DialogInterface.OnClickListener()  {
                public void onClick(DialogInterface dialog, int id) {
                    String email = editTextEmail.getText().toString();
                    String firstName = editTextFirstName.getText().toString();
                    String lastName = editTextLastName.getText().toString();
                    String phone = editTextPhone.getText().toString();
                    String address = editTextAddress.getText().toString();
                    ArrayList<String> diseases = new ArrayList<>();
                    if(diseasesListViewAdapter != null && !diseasesListViewAdapter.isEmpty()){
                        diseases.add(diseasesListViewAdapter.getItem(0)._id);
                        int lenght = diseasesListViewAdapter.getCount();
                        for (int i=1; i<lenght; i++){
                            diseases.add(diseasesListViewAdapter.getItem(i)._id);
                        }
                    }
                    Profile profile = new Profile<String>();
                    profile.email = email;
                    profile.name.first = firstName;
                    profile.name.last = lastName;
                    profile.phone = phone;
                    profile.address = address;
                    profile.diseases = diseases;
                    UpdateUserProfile task = new UpdateUserProfile(ProfileActivity.this);
                    task.userId = HF.UserId;
                    task.token = HF.UserAccessToken;
                    task.profile = profile;
                    task.execute();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create();
            builder.show();


        }
    };

    private class GetUserProfileData extends GetUserProfileTask {
        ProgressDialog progressDialog;

        public GetUserProfileData(Context ctx) {
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_profile_gettingData));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, HttpResponse<Profile<Disease>>> result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                switch (result.data.code) {
                    case 200 :
                        setProfileData();
                        break;
                    default:
                        Toast.makeText(context, String.valueOf(result.data), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }

    private class UpdateUserProfile extends UpdateUserProfileTask {
        ProgressDialog progressDialog;

        public UpdateUserProfile(Context ctx) {
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_profile_updatingData));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, HttpResponse<Profile<Disease>>> result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                switch (result.data.code) {
                    case 200 :
                        setProfileData();
                        Toast.makeText(ProfileActivity.this, getString(R.string.phrase_profile_dataUpdated), Toast.LENGTH_SHORT);
                        break;
                    default:
                        Toast.makeText(context, String.valueOf(result.data), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }
}
