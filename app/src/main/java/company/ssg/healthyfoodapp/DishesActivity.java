package company.ssg.healthyfoodapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class DishesActivity extends AppCompatActivity {
    public static final String TAG = "DishesActivity";

    public RecyclerView recyclerView;
    public RecyclerView.Adapter dishesAdapter;
    String[] coordinates = new String[2];
    String restaurants = "";
    String withoutIngredients = "";
    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dishes);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            coordinates = extras.getStringArray("coordinates");
            restaurants = extras.getString("restaurants");
            //withIngredients = extras.getString("withIngredients");
            withoutIngredients = extras.getString("withoutIngredients");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.DishesActivityToolbar);
        setSupportActionBar(toolbar);

        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.DishesActivitySwipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "REFRESH");
                GetRecomendations task = new GetRecomendations(DishesActivity.this);
                task.coordinates = coordinates;
                task.restaurants = restaurants;
                //task.withIngredients = withIngredients;
                task.withoutIngredients = withoutIngredients;
                task.token = HF.UserAccessToken;
                task.execute();
            }

        });

        recyclerView = (RecyclerView)findViewById(R.id.DishesActivityReciclerViewDishesList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setAdapter(dishesAdapter);

        GetRecomendations task = new GetRecomendations(DishesActivity.this);
        task.coordinates = coordinates;
        task.restaurants = restaurants;
        //task.withIngredients = withIngredients;
        task.withoutIngredients = withoutIngredients;
        task.token = HF.UserAccessToken;
        task.execute();
    }

    class GetRecomendations extends GetRecomendationsTask {
        ProgressDialog progressDialog;

        public GetRecomendations(Context ctx) {
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DishesActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_gettingDishes));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, HttpResponse<ArrayList<Recommendation>>> result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            swipeContainer.setRefreshing(false);
            if(result.error != null){
                Helpers.responseToError(context, result.error);
            }else{
                if(result.data != null){
                    switch (result.data.code) {
                        case 200 :
                            if(result.data.data.isEmpty()) {
                                Toast.makeText(getApplicationContext(), "No hemos encontrado platos que concuerden con tu criterio de busqueda.", Toast.LENGTH_SHORT).show();
                            }
                            dishesAdapter = new DishesAdapter(R.layout.dish_list_item, DishesActivity.this, DishesActivity.this);
                            recyclerView.setAdapter(dishesAdapter);

                            break;
                        default :
                            Toast.makeText(getApplicationContext(), String.valueOf(result.data.code), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }
    }
}
