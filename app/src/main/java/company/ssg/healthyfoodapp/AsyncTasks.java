package company.ssg.healthyfoodapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

class LoginTask extends AsyncTask<String, Void, Res<Error, Integer>> {
    public static final String TAG = "LoginTask";
    public String email, password;

    Context context;
    public LoginTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, Integer> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, Integer>>() {
            @Override
            public Res<Error, Integer> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/auth");
                String bodyParams = "email="+email+"&password="+password+"&rol=client";
                httpClient.setMethod("POST");
                httpClient.addParams(bodyParams);
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        String token = jsonResponse.getString("token");
                        String userId = jsonResponse.getString("userId");
                        SharedPreferences sp = context.getSharedPreferences(HF.SHARED_PREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("userEmail", email);
                        editor.putString("userId", userId);
                        editor.putString("userAccessToken", token);
                        editor.apply();
                        HF.UserEmail = email;
                        HF.UserAccessToken = token;
                        HF.UserId = userId;
                    }
                }
                return new Res(null, httpResponseCode);
            }
        });
    }
}

class SignUpTask extends AsyncTask<String, Void, Res<Error, Integer>> {
    public static final String TAG = "SignUpTask";

    public String email, password;
    Context context;

    public SignUpTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, Integer> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, Integer>>() {
            @Override
            public Res<Error, Integer> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/clients");
                String bodyParams = "email="+email+"&password="+password;
                httpClient.setMethod("POST");
                httpClient.addParams(bodyParams);
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                Log.i(TAG, response);
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 201) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        String token = jsonResponse.getString("token");
                        JSONObject client = jsonResponse.getJSONObject("clientCreate");
                        String userId = client.getString("_id");
                        SharedPreferences sp = context.getSharedPreferences(HF.SHARED_PREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("userEmail", email);
                        editor.putString("userId", userId);
                        editor.putString("userAccessToken", token);
                        editor.apply();
                        HF.UserEmail = email;
                        HF.UserAccessToken = token;
                        HF.UserId = userId;
                    }
                }
                return new Res(null, httpResponseCode);
            }
        });
    }
}

class GetRestaurantsTask extends AsyncTask<String, Void, Res<Error, HttpResponse<ArrayList<Restaurant>>>> {
    public static final String TAG = "GetRestaurantsTask";

    public String token;
    Context context;
    private Gson gson = new Gson();

    public GetRestaurantsTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<ArrayList<Restaurant>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<ArrayList<Restaurant>>>>() {
            @Override
            public Res<Error, HttpResponse<ArrayList<Restaurant>>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/restaurants");
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        Restaurant[] restaurants = gson.fromJson(jsonResponse.getJSONArray("restaurants").toString(), Restaurant[].class);
                        HF.restaurants = new ArrayList<>(Arrays.asList(restaurants));
                    }
                }
                return new Res(null, new HttpResponse(httpResponseCode, HF.restaurants));
            }
        });
    }
}

class GetDiseasesTask extends AsyncTask<String, Void, Res<Error, HttpResponse<ArrayList<Disease>>>> {
    public static final String TAG = "GetDiseasesTask";

    public String token;
    Context context;
    private Gson gson = new Gson();

    public GetDiseasesTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<ArrayList<Disease>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<ArrayList<Disease>>>>() {
            @Override
            public Res<Error, HttpResponse<ArrayList<Disease>>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/diseases");
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                //Log.i(TAG, response);
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        Disease[] diseases = gson.fromJson(jsonResponse.getJSONArray("diseases").toString(), Disease[].class);
                        HF.diseases = new ArrayList<>(Arrays.asList(diseases));
                        Log.i(TAG,gson.toJson(HF.diseases).toString());
                    }
                }
                return new Res(null, new HttpResponse(httpResponseCode, HF.diseases));
            }
        });
    }
}

class GetIngredientsTask extends AsyncTask<String, Void, Res<Error, HttpResponse<ArrayList<Ingredient>>>> {
    public static final String TAG = "GetIngredientsTask";

    public String token;
    Context context;
    private Gson gson = new Gson();

    public GetIngredientsTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<ArrayList<Ingredient>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<ArrayList<Ingredient>>>>() {
            @Override
            public Res<Error, HttpResponse<ArrayList<Ingredient>>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/ingredients");
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        Ingredient[] ingredients = gson.fromJson(jsonResponse.getJSONArray("ingredients").toString(), Ingredient[].class);
                        HF.ingredients = new ArrayList<>(Arrays.asList(ingredients));
                    }
                }
                return new Res(null, new HttpResponse(httpResponseCode, HF.ingredients));
            }
        });
    }
}

/*class GetDishesTask extends AsyncTask<String, Void, Res<Error, HttpResponse<ArrayList<Dish>>>> {
    public static final String TAG = "GetDiseasesTask";

    public String token;
    Context context;
    private Gson gson = new Gson();

    public GetDishesTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<ArrayList<Dish>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<ArrayList<Dish>>>>() {
            @Override
            public Res<Error, HttpResponse<ArrayList<Dish>>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/plates");
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                //System.out.println(response);
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        DishWithRestaurant[] dishes = gson.fromJson(jsonResponse.getJSONArray("plates").toString(), DishWithRestaurant[].class);
                        HF.dishes = new ArrayList<>(Arrays.asList(dishes));
                        System.out.println(gson.toJson(HF.dishes));
                    }
                }
                return new Res(null, new HttpResponse<>(httpResponseCode, HF.dishes));
            }
        });
    }
}*/

class GetDishRatingTask extends AsyncTask<String, Void, Res<Error, HttpResponse<Float>>> {
    public static final String TAG = "GetDishRatingTask";

    public String token, userId, dishId;
    Context context;

    public GetDishRatingTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<Float>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<Float>>>() {
            @Override
            public Res<Error, HttpResponse<Float>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/ratings?client="+userId+"&plate="+dishId);
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                //System.out.println(response);
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                float value = 0;
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONArray jsonResponse = new JSONArray(response);
                        if(jsonResponse.length() > 0){
                            JSONObject obj = jsonResponse.getJSONObject(0);
                            value = obj.getLong("value");
                        }
                    }
                }
                return new Res(null, new HttpResponse<>(httpResponseCode, value));
            }
        });
    }
}

class UpdateDishRatingTask extends AsyncTask<String, Void, Res<Error, HttpResponse<Dish>>> {
    public static final String TAG = "UpdateDishRatingTask";

    public String token, dishId, dishRating;
    Context context;
    private Gson gson = new Gson();

    public UpdateDishRatingTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<Dish>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<Dish>>>() {
            @Override
            public Res<Error, HttpResponse<Dish>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/"+dishId+"/ratings");
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("POST");
                httpClient.addParams("value="+dishRating);
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                //System.out.println(response);
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                Dish dish = null;
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        //JSONObject jsonResponse = new JSONObject(response);
                        dish = gson.fromJson(response, Dish.class);
                        System.out.println(gson.toJson(dish));
                    }
                }
                return new Res<>(null, new HttpResponse<>(httpResponseCode, dish));
            }
        });
    }
}

class GetUserProfileTask extends AsyncTask<String, Void, Res<Error, HttpResponse<Profile<Disease>>>> {
    public static final String TAG = "GetUserProfileTask";

    public String token, userId;
    Context context;
    private Gson gson = new Gson();

    public GetUserProfileTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<Profile<Disease>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<Profile<Disease>>>>() {
            @Override
            public Res<Error, HttpResponse<Profile<Disease>>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/clients/"+userId);
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        HF.userProfile = gson.fromJson(jsonResponse.getJSONObject("client").toString(), new TypeToken<Profile<Disease>>() {}.getType());
                    }
                }
                return new Res<>(null, new HttpResponse<>(httpResponseCode, HF.userProfile));
            }
        });
    }
}

class UpdateUserProfileTask extends AsyncTask<String, Void, Res<Error, HttpResponse<Profile<Disease>>>> {
    public static final String TAG = "UpdateUserProfileTask";

    public String token, userId;
    public Profile<String> profile;
    Context context;
    private Gson gson = new Gson();

    public UpdateUserProfileTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<Profile<Disease>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<Profile<Disease>>>>() {
            @Override
            public Res<Error, HttpResponse<Profile<Disease>>> call() throws Exception {
                HTTPClient httpClient = new HTTPClient(HF.API_SERVER_HOST+"/api/clients/"+userId);
                httpClient.setMethod("PUT");
                httpClient.addHeader("content-type", "application/json; charset=utf-8");
                httpClient.addHeader("Authorization", token);
                String data = gson.toJson(profile);
                httpClient.addParams(data);
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                //Log.i(TAG, response);
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONObject jsonResponse = new JSONObject(response);
                        HF.userProfile = gson.fromJson(jsonResponse.getJSONObject("client").toString(), new TypeToken<Profile<Disease>>() {}.getType());
                    }
                }
                return new Res<>(null, new HttpResponse<>(httpResponseCode, HF.userProfile));
            }
        });
    }
}

class GetRecomendationsTask extends AsyncTask<String, Void, Res<Error, HttpResponse<ArrayList<Recommendation>>>> {
    public static final String TAG = "GetDiseasesTask";

    public String[] coordinates = new String[2];
    public String restaurants = "";
    //public String withIngredients = "";
    public String withoutIngredients = "";
    public String token;
    Context context;
    private Gson gson = new Gson();

    public GetRecomendationsTask(Context ctx){
        context = ctx;
    }

    @Override
    final protected Res<Error, HttpResponse<ArrayList<Recommendation>>> doInBackground(String... params) {
        return Helpers.doTry(new Callable<Res<Error, HttpResponse<ArrayList<Recommendation>>>>() {
            @Override
            public Res<Error, HttpResponse<ArrayList<Recommendation>>> call() throws Exception {
                String url = HF.API_SERVER_HOST+"/api/recommendation?longitude="+coordinates[0]+"&latitude="+coordinates[1];
                if(!restaurants.isEmpty()){
                    url += "&restaurants="+restaurants;
                }
                /*if(!withIngredients.isEmpty()){
                    url += "&withIngredients="+withIngredients;
                }*/
                if(!withoutIngredients.isEmpty()){
                    url += "&withoutIngredients="+withoutIngredients;
                }
                HTTPClient httpClient = new HTTPClient(url);
                httpClient.addHeader("content-type", "application/json; charset=utf-8");
                httpClient.addHeader("Authorization", token);
                httpClient.setMethod("GET");
                httpClient.makeRequest();
                String response = HTTPClient.getStringFromResponse(httpClient.getHttpResponse());
                Log.i(TAG,response);
                int httpResponseCode = httpClient.getConnection().getResponseCode();
                if (httpResponseCode == 200) {
                    if(httpClient.getConnection().getHeaderField("Content-Type").equals("application/json; charset=utf-8")) {
                        JSONArray jsonResponse = new JSONArray(response);
                        Recommendation[] recommendations = gson.fromJson(jsonResponse.toString(), Recommendation[].class);
                        HF.recommendations = new ArrayList<>(Arrays.asList(recommendations));
                        Log.i(TAG,gson.toJson(HF.recommendations));
                    }
                }
                return new Res(null, new HttpResponse<>(httpResponseCode, HF.recommendations));
            }
        });
    }
}

