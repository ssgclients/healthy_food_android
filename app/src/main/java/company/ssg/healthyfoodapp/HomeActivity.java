package company.ssg.healthyfoodapp;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements LocationListener {
    public static final String TAG = "HomeActivity";

    int LIST_VIEW_HEIGHT;
    SharedPreferences sharedPreferences;

    //Show custom filter widgets
    Button buttonCustomFilter;
    LinearLayout linearLayoutCustomFilter;

    //Restaurants filter widgets
    AutoCompleteTextView textViewRestaurant;
    ImageButton buttonAddRestaurant;
    ListView listViewRestaurants;
    RestaurantsAdapter restaurantsAdapter;

    //Without ingredients filter widgets
    AutoCompleteTextView textViewWithoutIngredients;
    ImageButton buttonAddWithoutIngredients;
    ListView listViewWithoutIngredients;
    IngredientsAdapter withoutIngredientsAdapter;

    Button buttonRecommendDishes;

    LocationManager locationManager;

    Gson gson;

    boolean saveInSF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        saveInSF = true;

        gson = new Gson();
        Log.i(TAG, HF.UserAccessToken);
        sharedPreferences = getSharedPreferences(HF.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        HF.UserId = sharedPreferences.getString("userId", "");

        GetRestaurantsTask task1 = new GetRestaurantsTask(this);
        task1.token = HF.UserAccessToken;
        task1.execute();
        GetIngredientsTask task2 = new GetIngredientsTask(this);
        task2.token = HF.UserAccessToken;
        task2.execute();
        GetDiseasesTask task3 = new GetDiseasesTask(this);
        task3.token = HF.UserAccessToken;
        task3.execute();
        GetUserProfileTask task4 = new GetUserProfileTask(this);
        task4.userId = HF.UserId;
        task4.token = HF.UserAccessToken;
        task4.execute();

        LIST_VIEW_HEIGHT = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());

        Toolbar toolbar = (Toolbar) findViewById(R.id.HomeActivityToolbar);
        setSupportActionBar(toolbar);

        buttonCustomFilter = (Button) findViewById(R.id.HomeButtonCustomFilter);
        buttonCustomFilter.setOnClickListener(onClickButtonCustomFilter);
        linearLayoutCustomFilter = (LinearLayout) findViewById(R.id.HomeLinearLayoutCustomFilter);

        //Add restaurants to filter
        textViewRestaurant = (AutoCompleteTextView) findViewById(R.id.HomeTextViewRestaurants);
        textViewRestaurant.setAdapter(new RestaurantsAdapter(this));
        buttonAddRestaurant = (ImageButton) findViewById(R.id.HomeButtonAddRestaurant);
        buttonAddRestaurant.setOnClickListener(onClickButtonAddRestaurant);
        restaurantsAdapter = new RestaurantsAdapter(this);
        listViewRestaurants = (ListView) findViewById(R.id.HomeListViewRestaurants);
        listViewRestaurants.setAdapter(restaurantsAdapter);
        listViewRestaurants.setOnTouchListener(onTouchListViewRestaurants);
        listViewRestaurants.setOnItemLongClickListener(onItemRestaurantLongClickListener);

        //Add ingredients to withoutIngredients filter
        textViewWithoutIngredients = (AutoCompleteTextView) findViewById(R.id.HomeTextViewWithoutIngredients);
        textViewWithoutIngredients.setAdapter(new IngredientsAdapter(this));
        buttonAddWithoutIngredients = (ImageButton) findViewById(R.id.HomeButtonAddWithoutIngredients);
        buttonAddWithoutIngredients.setOnClickListener(onClickButtonAddWithoutIngredients);
        withoutIngredientsAdapter = new IngredientsAdapter(this);
        listViewWithoutIngredients = (ListView) findViewById(R.id.HomeListViewWithoutIngredients);
        listViewWithoutIngredients.setAdapter(withoutIngredientsAdapter);
        listViewWithoutIngredients.setOnTouchListener(onTouchListViewWithoutIngredients);
        listViewWithoutIngredients.setOnItemLongClickListener(onItemWithoutIngredientsLongClickListener);

        buttonRecommendDishes = (Button) findViewById(R.id.HomeButtonRecommendDishes);

        buttonRecommendDishes.setOnClickListener(onClickButtonRecommendDishes);

        String restaurants = sharedPreferences.getString("filterRestaurants", "");
        Log.i(TAG, "restaurants: " + restaurants);
        String withoutIngredients = sharedPreferences.getString("filterWithoutIngredients", "");
        Log.i(TAG, "withoutIngredients: " + withoutIngredients);

        Restaurant[] restaurantFromSP = gson.fromJson(restaurants, Restaurant[].class);
        Log.i(TAG, gson.toJson(restaurantFromSP).toString());
        if (restaurantFromSP != null && restaurantFromSP.length > 0) {
            Log.i(TAG, "Restaurants not empty");
            restaurantsAdapter.addAll(restaurantFromSP);
            if (!restaurantsAdapter.isEmpty()) {
                listViewRestaurants.setVisibility(View.VISIBLE);
            }
            if (restaurantsAdapter.getCount() > 1) {
                listViewRestaurants.getLayoutParams().height = LIST_VIEW_HEIGHT;
            }
        }
        Ingredient[] woiFromSP = gson.fromJson(withoutIngredients, Ingredient[].class);
        Log.i(TAG, gson.toJson(woiFromSP).toString());
        if (woiFromSP != null && woiFromSP.length > 0) {
            Log.i(TAG, "woiFromSP not empty");
            withoutIngredientsAdapter.addAll(woiFromSP);
            if (!withoutIngredientsAdapter.isEmpty()) {
                listViewWithoutIngredients.setVisibility(View.VISIBLE);
            }
            if (withoutIngredientsAdapter.getCount() > 1) {
                listViewWithoutIngredients.getLayoutParams().height = LIST_VIEW_HEIGHT;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.i(TAG, "Requesting for permissions");
            requestPermissions(permission, 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopGPS();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (saveInSF) {
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            int count = restaurantsAdapter.getCount();
            for (int i = 0; i < count; i++) {
                restaurants.add(restaurantsAdapter.getItem(i));
            }
            ArrayList<Ingredient> woi = new ArrayList<>();
            count = withoutIngredientsAdapter.getCount();
            for (int i = 0; i < count; i++) {
                woi.add(withoutIngredientsAdapter.getItem(i));
            }

            SharedPreferences sp = getSharedPreferences(HF.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("filterRestaurants", gson.toJson(restaurants).toString());
            editor.putString("filterWithoutIngredients", gson.toJson(woi).toString());
            editor.apply();

            Log.i(TAG, "Filter saved in Shared Preferences");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.HomeMenuProfile:
                Intent i = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(i);
                return true;
            /*case R.id.HomeMenuDishes:
                Intent a = new Intent(HomeActivity.this, DishesActivity.class);
                startActivity(a);
                return true;*/
            case R.id.HomeMenuLogout:
                Log.i(TAG, "Logout");
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setMessage("¿Quieres cerrar sesion?");
                builder.setTitle("Confirmacion");
                builder.setPositiveButton("Cerrar sesion", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i(TAG, "Loging out");
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.apply();
                        editor.commit();
                        saveInSF = false;
                        Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(i);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create();
                builder.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                startGPS();
                break;
        }
    }

    private void startGPS() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Starting location manager");
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("GPS Desabilitado");
                alertDialogBuilder.setMessage("Esta aplicacion necesita que el GPS este habilidato para poder funcionar correctamente.\n¿Desea habilitar el GPS?");
                alertDialogBuilder.setPositiveButton("Habilitar GPS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(callGPSSettingIntent, 1);
                    }
                });
                alertDialogBuilder.setNegativeButton("Cerrar aplicacion", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
                AlertDialog alert = alertDialogBuilder.create();
                alert.show();
            } else {
                Log.i(TAG, "Requesting location updates");
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
                Log.i(TAG, "Done");
            }
        } else {
            String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Log.i(TAG, "Requesting for permissions");
                requestPermissions(permission, 0);
            }
        }
    }

    private void stopGPS() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if(locationManager != null){
                locationManager.removeUpdates(this);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == 0){
            startGPS();
        }
    }

    private View.OnClickListener onClickButtonCustomFilter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(linearLayoutCustomFilter.getVisibility() == View.GONE) {
                linearLayoutCustomFilter.setVisibility(View.VISIBLE);
                buttonCustomFilter.setText(getString(R.string.phrase_hideCustomFilter));
            }else if(linearLayoutCustomFilter.getVisibility() == View.VISIBLE) {
                linearLayoutCustomFilter.setVisibility(View.GONE);
                buttonCustomFilter.setText(getString(R.string.phrase_showCustomFilter));
            }
        }
    };

    private View.OnClickListener onClickButtonAddRestaurant = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = textViewRestaurant.getText().toString();
            if(name.isEmpty()){
                Toast.makeText(HomeActivity.this,
                        getString(R.string.phrase_restaurantNameRequired),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            Restaurant selectedRestaurant = null;
            if(HF.restaurants != null){
                int length = HF.restaurants.size();
                for (int i = 0; i< length; i++){
                    if(HF.restaurants.get(i).name.equals(name)){
                        selectedRestaurant = HF.restaurants.get(i);
                    }
                }
            }
            if(selectedRestaurant == null){
                Toast.makeText(HomeActivity.this,
                        getString(R.string.phrase_restaurantNotFoud),
                        Toast.LENGTH_SHORT).show();
                return;
            }
            int length = restaurantsAdapter.getCount();
            for (int i = 0; i< length; i++){
                if(restaurantsAdapter.getItem(i).name.equals(name)){
                    Toast.makeText(HomeActivity.this,
                            getString(R.string.phrase_restaurantAlreadyAdded),
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            restaurantsAdapter.add(selectedRestaurant);
            if(!restaurantsAdapter.isEmpty()){
                listViewRestaurants.setVisibility(View.VISIBLE);
            }
            if(restaurantsAdapter.getCount() > 1){
                listViewRestaurants.getLayoutParams().height = LIST_VIEW_HEIGHT;
            }
            textViewRestaurant.setText("");
        }
    };

    private View.OnTouchListener onTouchListViewRestaurants = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    private AdapterView.OnItemLongClickListener onItemRestaurantLongClickListener = new AdapterView.OnItemLongClickListener() {
        int  pos = -1;
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            pos = position;
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setMessage(getString(R.string.phrase_deleteRestaurantConfirmation));
            builder.setTitle(getString(R.string.word_confirmation));
            builder.setPositiveButton(getString(R.string.word_delete), new DialogInterface.OnClickListener()  {
                public void onClick(DialogInterface dialog, int id) {
                    restaurantsAdapter.remove(restaurantsAdapter.getItem(pos));
                    if(restaurantsAdapter.getCount() == 1) {
                        listViewRestaurants.getLayoutParams().height = ListView.LayoutParams.WRAP_CONTENT;
                    }
                    if(restaurantsAdapter.isEmpty()){
                        listViewRestaurants.setVisibility(View.GONE);
                    }
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create();
            builder.show();
            return true;
        }
    };

    private View.OnClickListener onClickButtonAddWithoutIngredients = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = textViewWithoutIngredients.getText().toString();
            if(name.isEmpty()){
                Toast.makeText(HomeActivity.this, getString(R.string.phrase_ingredientNameRequired),Toast.LENGTH_SHORT).show();
                return;
            }
            Ingredient selectedIngredient = null;
            if(HF.ingredients != null){
                int length = HF.ingredients.size();
                for (int i = 0; i< length; i++){
                    if(HF.ingredients.get(i).name.equals(name)){
                        selectedIngredient = HF.ingredients.get(i);
                    }
                }
            }
            if(selectedIngredient == null){
                Toast.makeText(HomeActivity.this, getString(R.string.phrase_ingredientNotFoud),Toast.LENGTH_SHORT).show();
                return;
            }
            int length = withoutIngredientsAdapter.getCount();
            for (int i = 0; i< length; i++){
                if(withoutIngredientsAdapter.getItem(i).name.equals(name)){
                    Toast.makeText(HomeActivity.this, getString(R.string.phrase_ingredientAlreadyAdded),Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            withoutIngredientsAdapter.add(selectedIngredient);
            if(withoutIngredientsAdapter.getCount() > 1){
                listViewWithoutIngredients.getLayoutParams().height = LIST_VIEW_HEIGHT;
            }
            if(!withoutIngredientsAdapter.isEmpty()){
                listViewWithoutIngredients.setVisibility(View.VISIBLE);
            }
            textViewWithoutIngredients.setText("");
        }
    };

    private View.OnTouchListener onTouchListViewWithoutIngredients = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    private AdapterView.OnItemLongClickListener onItemWithoutIngredientsLongClickListener = new AdapterView.OnItemLongClickListener() {
        int  pos = -1;
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            Log.i(TAG, "Long Click");
            pos = position;
            Log.i(TAG, String.valueOf(position));
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setMessage(getString(R.string.phrase_deleteIngredientConfirmation));
            builder.setTitle(getString(R.string.word_confirmation));
            builder.setPositiveButton(getString(R.string.word_delete), new DialogInterface.OnClickListener()  {
                public void onClick(DialogInterface dialog, int id) {
                    withoutIngredientsAdapter.remove(withoutIngredientsAdapter.getItem(pos));
                    if(withoutIngredientsAdapter.getCount() == 1) {
                        listViewWithoutIngredients.getLayoutParams().height = ListView.LayoutParams.WRAP_CONTENT;
                    }
                    if(withoutIngredientsAdapter.isEmpty()){
                        listViewWithoutIngredients.setVisibility(View.GONE);
                    }
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create();
            builder.show();
            return true;
        }
    };

    private View.OnClickListener onClickButtonRecommendDishes = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "Recommending dishes");
            String restaurants = "";
            //String withIngredients = "";
            String withoutIngredients = "";
            if(HF.MyCurretLocation == null){
                Toast.makeText(HomeActivity.this, "Actualmente no tenemos tu posicion por lo tanto no podemos recomendarte los platos", Toast.LENGTH_SHORT).show();
                return;
            }

            if(restaurantsAdapter != null && !restaurantsAdapter.isEmpty()){
                restaurants = restaurantsAdapter.getItem(0)._id;
                int lenght = restaurantsAdapter.getCount();
                for (int i=1; i<lenght; i++){
                    restaurants += ","+restaurantsAdapter.getItem(i)._id;
                }
            }
            if(withoutIngredientsAdapter != null && !withoutIngredientsAdapter.isEmpty()){
                withoutIngredients = withoutIngredientsAdapter.getItem(0).name;
                int lenght = withoutIngredientsAdapter.getCount();
                for (int i=1; i<lenght; i++){
                    withoutIngredients += ","+withoutIngredientsAdapter.getItem(i).name;
                }
            }
            Log.i(TAG, restaurants);
            //Log.i(TAG, withIngredients);
            Log.i(TAG, withoutIngredients);
            Log.i(TAG, "Lat: "+HF.MyCurretLocation.getLatitude());
            Log.i(TAG, "Lon: "+HF.MyCurretLocation.getLongitude());

            //GetRecommendations task = new GetRecommendations(HomeActivity.this);
            //task.token = HF.UserAccessToken;
            String[] coordinates = new String[2];
            coordinates[0] = String.valueOf(HF.MyCurretLocation.getLongitude());
            coordinates[1] = String.valueOf(HF.MyCurretLocation.getLatitude());
            //task.coordinates = coordinates;
            //task.execute();*/
            Intent i = new Intent(HomeActivity.this, DishesActivity.class);
            i.putExtra("coordinates", coordinates);
            i.putExtra("restaurants", restaurants);
            //i.putExtra("withIngredients", withIngredients);
            i.putExtra("withoutIngredients", withoutIngredients);
            startActivity(i);
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "Lat: "+location.getLatitude()+ ", lon: "+location.getLongitude());
        HF.MyCurretLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        if (provider.equals("gps")){
            startGPS();
        }
    }
}
